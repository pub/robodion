#include "M5Atom.h"
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "SPIFFS.h"
#include "FS.h"

//setPWMを使う場合のテンプレです．
//万能ではないです
//適宜変更お願いします．

//I2C通信 7bitアドレス指定
//デフォルト → 0b100_0000 = 0x40
//下位6bitをはんだ付けで決める（はんだ有→1，はんだ無→0）
Adafruit_PWMServoDriver pwm1 = Adafruit_PWMServoDriver(0x40); //はんだ付け無し
Adafruit_PWMServoDriver pwm2 = Adafruit_PWMServoDriver(0x41); //一番右だけはんだ付け → 0b100_0001 = 0x41


//変更の必要なし
#define SERVO_FREQ 80                   //PWM信号の周波数（80Hz）
#define INTERNAL_CLK_FREQ 27000000      //サーボドライバー内部クロック周波数（27MHz）


//適宜変更してください
#define NOTE_NUM_MIN 0     //最低音のノート番号　例）アルトサックスは49（C#3）が最低音
#define NOTE_NUM_MAX 31    //最高音のノート番号　例）アルトサックスは81（A5）が最高音
#define MOTOR_NUM 32       //モーターの個数
#define INSTRUMENT_NUM 0   //楽器番号（音ゲー・システムの楽器識別用）


//GUIで調整した値を書き込む
//面倒ならMakeFileしてもOK

int ang[MOTOR_NUM][2] = {
  
//↓MakeFile用コメント 消さないで！
//GUI_start_point

  {350, 400}, //ch0(Driver1_0)
  {350, 400}, //ch1
  {350, 400}, //ch2
  {350, 400}, //ch3
  
  {350, 400}, //ch4
  {350, 400}, //ch5
  {350, 400}, //ch6
  {350, 400}, //ch7
  
  {350, 400}, //ch8
  {350, 400}, //ch9
  {350, 400}, //ch10
  {350, 400}, //ch11
  
  {350, 400}, //ch12
  {350, 400}, //ch13
  {350, 400}, //ch14
  {350, 400}, //ch15(Driver1_15)

  {350, 400}, //ch16(Driver2_0)
  {350, 400}, //ch17
  {350, 400}, //ch18
  {350, 400}, //ch19
  
  {350, 400}, //ch20
  {350, 400}, //ch21
  {350, 400}, //ch22
  {350, 400}, //ch23
  
  {350, 400}, //ch24
  {350, 400}, //ch25
  {350, 400}, //ch26
  {350, 400}, //ch27

  {350, 400}, //ch28
  {350, 400}, //ch29
  {350, 400}, //ch30
  {350, 400}, //ch31(Driver2_15)
  
//GUI_end_point
//↑MakeFile用コメント 消さないで！

};



//GUI，音ゲー・システム関連**********************************************

void setting_mode() {
  unsigned char type, ch, mm, v1, v2;
  int value;
  
  while (1) {
    if (Serial.available() > 0) {
      unsigned char buf[16];
      Serial.readBytes(buf, 1);
      type = buf[0];
      switch (type) {
        
        case 1: //GUIで更新した値をangに代入し，モーターを回転させる
          Serial.readBytes(buf, 1);
          ch = buf[0];
          Serial.readBytes(buf, 1);
          mm = buf[0];
          Serial.readBytes(buf, 1);
          v1 = buf[0];
          Serial.readBytes(buf, 1);
          v2 = buf[0];
          value = v1 * 256 + v2;
          if (ch <= MOTOR_NUM && mm <= 2 && value < 2048) {
            ang[ch][mm] = value;
            if (ch < 16)
              pwm1.setPWM(ch, 0, ang[ch][mm]);
            else
              pwm2.setPWM(ch-16, 0, ang[ch][mm]);
          }
          return;

        case 4: //GUIにangの値を送信する
          Serial.readBytes(buf, 1);
          ch = buf[0];
          Serial.readBytes(buf, 1);
          mm = buf[0];
          if (ch >= 0 && ch <= MOTOR_NUM && mm <= 2) {
            buf[0] = ang[ch][mm] / 256;
            buf[1] = ang[ch][mm] % 256;
            Serial.write(buf, 2);
          }
          return;

        case 5: //音ゲー・システムの楽器識別用
          buf[0] = INSTRUMENT_NUM;
          Serial.write(buf, 1);
          return;
        
        default:
          return;
      }
    }
  }
}

//*****************************************************************

void Note_On(unsigned char n){  //n = （受信したノート番号）-（最低音のノート番号）
  if (n < 16){
    pwm1.setPWM(n, 0, ang[n][1]);
  }else{
    pwm2.setPWM(n-16, 0, ang[n][1]);
  }
}

void Note_Off(unsigned char n){
  if (n < 16){
    pwm1.setPWM(n, 0, ang[n][0]);
  }else{
    pwm2.setPWM(n-16, 0, ang[n][0]);
  }
}

void setup()
{
  M5.begin(true, true, true);
  delay(50);
  M5.dis.clear();

  pwm1.begin();
  pwm1.setOscillatorFrequency(INTERNAL_CLK_FREQ);
  pwm1.setPWMFreq(SERVO_FREQ);
  delay(10);

  pwm2.begin();
  pwm2.setOscillatorFrequency(INTERNAL_CLK_FREQ);
  pwm2.setPWMFreq(SERVO_FREQ);
  delay(10);

  Serial.begin(115200);
  while (!Serial) {
    ;
  }
}

void loop()
{
  unsigned char type, noteNum, velocity;

  while (1) {
    if (Serial.available() > 0) {
      unsigned char buf[3];
      Serial.readBytes(buf, 1);
      type = buf[0];
      switch (type) {

        //ノートオン（MIDIデータフォーマット要参照）
        case 0x90:
          Serial.readBytes(buf, 1);
          noteNum = buf[0];
          Serial.readBytes(buf, 1);
          velocity = buf[0];
          if (NOTE_NUM_MIN <= noteNum && noteNum <= NOTE_NUM_MAX){
            Note_On(noteNum-NOTE_NUM_MIN);
          }
          break;

        //ノートオフ（MIDIデータフォーマット要参照）
        case 0x80:
          Serial.readBytes(buf, 1);
          noteNum = buf[0];
          Serial.readBytes(buf, 1);
          velocity = buf[0];
          if (NOTE_NUM_MIN <= noteNum && noteNum <= NOTE_NUM_MAX){
            Note_Off(noteNum-NOTE_NUM_MIN);
          }
          break;
        
        //GUI or 音ゲー・システムから呼び出し
        case 0x9F:
          Serial.readBytes(buf, 1);
          noteNum = buf[0];
          Serial.readBytes(buf, 1);
          velocity = buf[0];
          if (noteNum == 1 && velocity == 1){
            setting_mode();
          }
          break;
          
        default:
          break;
      }
    }
        
    delayMicroseconds(100);
  }
}
